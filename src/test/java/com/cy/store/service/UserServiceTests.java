package com.cy.store.service;

import com.cy.store.entity.User;
import com.cy.store.service.ex.ServiceExeption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTests {
    @Autowired
    private IUserService userService;

    @Test
    public void reg(){
        try {
            User user = new User();
            user.setUsername("test002");
            user.setPassword("123");
            userService.reg(user);
            System.out.println("OK");
        } catch (ServiceExeption e) {
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void login(){
        User user = userService.login("test001", "123");
        System.out.println(user);
    }

    @Test
    public void changePassword(){
        userService.changePassword(10, "管理员","123","321");
    }
}
