package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTests {
    @Autowired
    private UserMapper userMapper;


    @Test
    public void insert(){
        User user = new User();
        user.setUsername("tom");
        user.setPassword("123");


        System.out.println( userMapper.insert(user) );
    }

    @Test
    public void findByUsername(){
        User user = userMapper.findByUsername("tim");
        System.out.println(user);
    }

    @Test
    public void findByUid(){
        System.out.println(userMapper.findByUid(9));
    }

    @Test
    public void updatePasswordByUid(){
        System.out.println(userMapper.updatePasswordByUid(9, "123","管理员",new Date())
        );
    }


}
