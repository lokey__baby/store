package com.cy.store.service.impl;

import com.cy.store.entity.User;
import com.cy.store.mapper.UserMapper;
import com.cy.store.service.IUserService;
import com.cy.store.service.ex.InsertExeption;
import com.cy.store.service.ex.PasswordNotMatchException;
import com.cy.store.service.ex.UsernameDulplicatedExeption;
import com.cy.store.service.ex.UsernameNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public void reg(User user) {
        String username = user.getUsername();

        User result = userMapper.findByUsername(username);

        if(result!=null){
            throw new UsernameDulplicatedExeption("用户名被占用");

        }
        //密码加密
        String oldPassword = user.getPassword();
        String salt = UUID.randomUUID().toString().toUpperCase();

        String md5Password = getMD5Password(oldPassword, salt);

        user.setPassword(md5Password);

        //记录盐值
        user.setSalt(salt);

        //修改日志信息
        user.setIsDelete(0);
        user.setCreatedUser(user.getUsername());
        user.setModifiedUser(user.getUsername());
        Date date = new Date();
        user.setCreatedTime(date);
        user.setModifiedTime(date);



        Integer rows = userMapper.insert(user);

        if(rows != 1 ){
            throw new InsertExeption("用户注册过程中产生了未知的异常");
        }


    }

    @Override
    public User login(String username, String password) {
        User result = userMapper.findByUsername(username);
        if(result == null){
            throw new UsernameNotFoundException("用户数据不存在");
        }

        String salt = result.getSalt();
        String oldPassword = result.getPassword();
        String newMD5Password = getMD5Password(password, salt);
        if(!newMD5Password.equals(oldPassword)){
            throw new PasswordNotMatchException("用户密码错误");
        }

        if(result.getIsDelete() == 1){
            throw new UsernameNotFoundException("用户数据不存在");
        }
        User user = new User();
        user.setUid(result.getUid());
        user.setUsername(result.getUsername());
        user.setAvatar(result.getAvatar());

        return user;
    }

    @Override
    public void changePassword(Integer uid, String username, String oldPassword, String newPassword) {
        User result = userMapper.findByUid(uid);
        if(result == null || result.getIsDelete() == 1){
            throw new UsernameNotFoundException("用户数据不存在");
        }
        String oldMD5Password = getMD5Password(oldPassword, result.getSalt());
        if(!oldMD5Password.equals(result.getPassword())){
            throw new PasswordNotMatchException("密码错误");
        }

        String newMD5Password = getMD5Password(newPassword, result.getSalt());
        userMapper.updatePasswordByUid(result.getUid(), newMD5Password, username,new Date());
    }

    //加密算法
    private String getMD5Password(String password , String salt){

        for(int i=0 ; i<3; ++i){
            password = DigestUtils.md5DigestAsHex((salt+password+salt).getBytes()).toUpperCase();
        }
        return password;
    }

}
