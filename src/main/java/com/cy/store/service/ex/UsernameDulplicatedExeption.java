package com.cy.store.service.ex;

public class UsernameDulplicatedExeption extends ServiceExeption{
    public UsernameDulplicatedExeption() {
        super();
    }

    public UsernameDulplicatedExeption(String message) {
        super(message);
    }

    public UsernameDulplicatedExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameDulplicatedExeption(Throwable cause) {
        super(cause);
    }

    protected UsernameDulplicatedExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
