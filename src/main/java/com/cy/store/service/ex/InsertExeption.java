package com.cy.store.service.ex;

public class InsertExeption extends ServiceExeption{
    public InsertExeption() {
        super();
    }

    public InsertExeption(String message) {
        super(message);
    }

    public InsertExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public InsertExeption(Throwable cause) {
        super(cause);
    }

    protected InsertExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
