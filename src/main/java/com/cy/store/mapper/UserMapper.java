package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;

@Mapper
public interface UserMapper {

    Integer insert(User user);

    User findByUsername(String username);

    User findByUid(Integer uid);

    Integer updatePasswordByUid(Integer uid, String password, String modifiedUser, Date modifiedTime);
}
